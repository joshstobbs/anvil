import axios from 'axios'

class WpApi {
    constructor (siteUrl) {
        this.apiBase = `${siteUrl}/wp-json`
    }

    siteData () {
        return axios.get(this.apiBase)
            .then(response => {
                const {
                    name,
                    description,
                    url,
                    home,
                    gmt_offset,
                    timezone_string
                } = response.data

                return {
                    site_data: {
                        name,
                        description,
                        url,
                        home,
                        gmt_offset,
                        timezone_string
                    }
                }
            }).catch(errors => {
                return {
                    errors: errors
                } 
            })
    }

    posts (options) {
        const params = {
            page: 1,
            per_page: 5,
            ...options
        }

        return axios.get(`${this.apiBase}/wp/v2/posts`, { params })
            .then(response => {
                return {
                    posts: response.data
                }
            }).catch(errors => {
                return {
                    errors: errors
                }
            })
    }

    authors (options) {
        const params = {
            page: 1,
            per_page: 20,
            ...options
        }

        return axios.get(`${this.apiBase}/wp/v2/posts`, { params })
            .then(response => {
                return {
                    users: response.data
                }
            })
            .catch(errors => {
                errors: errors
            })
    }
}

const wp = new WpApi('https://admin.anvildesign.ca')

export default wp
