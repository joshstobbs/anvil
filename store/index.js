import Vuex from 'vuex'
import wp from '@/lib/wp'

// Mutation Types

export const types = {
    SITE_DATA_UPDATE: 'SITE_DATA_UPDATE',
    POST_LIST_UPDATE: 'POST_LIST_UPDATE',
    CURRENT_POST_UPDATE: 'CURRENT_POST_UPDATE',
    AUTHORS_UPDATE: 'AUTHORS_UPDATE'
}

const createStore = () => {

    return new Vuex.Store({
        state: {
            site_data: {},
            post_list: [],
            current_post: {},
            authors: {}
        },
        mutations: {
            [types.SITE_DATA_UPDATE] (state, payload) {
                state.site_data = { ...payload }
            },
            [types.POST_LIST_UPDATE] (state, payload) {
                state.post_list = [ ...payload ]
              },
            [types.CURRENT_POST_UPDATE] (state, payload) {
                state.current_post = { ...payload }
            },
            [types.AUTHORS_UPDATE] (state, payload) {
                state.authors = { ...payload }
            }
        },
        actions: {
            nuxtServerInit ({ commit }) {
                const getSiteData =  wp.siteData()
                    .then(response => {
                        commit(types.SITE_DATA_UPDATE, response.site_data)
                    })
                
                const getAuthors = wp.authors()
                    .then(response => {
                        const authors = response.users.reduce((out, val) => {
                            return {
                                ...out,
                                [val.id]: val
                            }
                        }, {})
                        commit(types.AUTHORS_UPDATE, authors)
                    })
                return Promise.all([getSiteData, getAuthors])
            }
        }
    })

}

export default createStore
